Welcome to bearlib-py's documentation!
======================================

.. toctree::
  :maxdepth: 2
  :caption: Contents:

.. include:: README.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* :ref:`bearlib`
